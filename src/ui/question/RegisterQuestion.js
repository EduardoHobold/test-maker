import React from 'react'
import { withRouter } from 'react-router-dom'

import questionService from '../../service/QuestionService'

class RegisterQuestion extends React.Component {

	constructor(props) {
		super(props)

		this.history = props.history
		this.questionService = new questionService()

		this.state = {
			testId: props.match.params.test,
			description: '',
			correct: -1,
			answers: []
		}
	}

	updateField(event) {
		this.setState({
			[event.target.name]: event.target.value
		})
	}

	updateOption(value, position) {
		let option = this.state.answers
		option[position] = value
		
		this.setState({
			answers: option
		})
	}

	async save() {
		let question = {
			description: this.state.description,
			test: this.state.testId,
			answers: this.state.answers
		}

		for (let i = 0; i < this.state.answers.length; i++) {
			question.answers[i] = {
				description: this.state.answers[i],
				correct: i === parseInt(this.state.correct)
			}
		}

		await this.questionService.save(question)
		this.history.push('/questions/' + this.state.testId)
	}


	render() {
		return (
			<div>

				<h2><b>Nova Questão</b></h2>
				<br />

				Descrição da questão: <br />
				<input type="text" size="35" name="description" onChange={event => this.updateField(event)}/> <br /><br />

				Respostas:(uma deve estar correta) <br />
                <input type="text" size="35" onChange={event => this.updateOption(event.target.value, 0)}/> 
                <input type="radio" value="0" name="correct" onChange={event => this.updateField(event)}/> Correta<br />

                <input type="text" size="35" onChange={event => this.updateOption(event.target.value, 1)}/> 
                <input type="radio" value="1" name="correct" onChange={event => this.updateField(event)}/> Correta<br />

                <input type="text" size="35" onChange={event => this.updateOption(event.target.value, 2)}/> 
                <input type="radio" value="2" name="correct" onChange={event => this.updateField(event)}/> Correta<br />

                <input type="text" size="35" onChange={event => this.updateOption(event.target.value, 3)}/> 
                <input type="radio" value="3" name="correct" onChange={event => this.updateField(event)}/> Correta<br />

                <input type="text" size="35" onChange={event => this.updateOption(event.target.value, 4)}/> 
                <input type="radio" value="4" name="correct" onChange={event => this.updateField(event)}/> Correta<br />
				<br />
                <input type="button" value="Salvar" onClick={() => this.save()}/>

			</div>
		)
	}
}

export default withRouter(RegisterQuestion)