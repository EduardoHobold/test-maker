import React from 'react'
import { withRouter } from 'react-router-dom'

import QuestionService from '../../service/QuestionService'

class Questions extends React.Component {

	constructor(props) {
		super(props)

		this.history = props.history
		this.questionService = new QuestionService()

		this.state = {
			testId: props.match.params.test,
			listQuestion: []
		}
	}

	async componentDidMount() {
		let result = await this.questionService.findByTest(this.state.testId)
		this.setState({
			listQuestion: result
		})
	}

	openRegister(testId) {
        this.history.push('/register-question/' + testId)
    }

	render() {
		return (

			<div>
				{this.state.listQuestion.map(q => (
					<div>
						{q.description} <br />
						<ul>
							{
								q.answers.map(a => (
									<li>{a.description}</li>
								))
							}
						</ul>
					</div>
				))}


				<br />
				<input type="button" value="Nova Questão" onClick={() => this.openRegister(this.state.testId)} />
			</div>
		)
	}

}

export default withRouter(Questions)